(define-module (emilua packages native-packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages gcc) ;for gcc-10
  #:use-module (gnu packages lua)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages pkg-config)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emilua-glib
  (package
   (name "emilua-glib")
   (version "0.2.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/emilua/glib.git")
                  (commit (string-append "v" version))))
            (sha256
             (base32
              "0mz1d2wh32ql3qa51nvgdihs1xi6ldcpc5skrmyyyv5bg8y1ms5s"))))
   (build-system meson-build-system)
   (inputs
    `(("emilua" ,emilua)
      ("luajit-lua52-openresty" ,luajit-lua52-openresty)
      ("fmt" ,fmt)
      ("glib" ,glib)
      ("boost" ,boost)))
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ("gcc" ,gcc-10)))
   (synopsis "GLib event loop integration to Emilua")
   (description "GLib event loop integration to Emilua")
   (home-page "https://gitlab.com/emilua/glib")
   (license license:x11)))
